package com.cryptobucksapp.testcryptobucks


import android.os.Bundle
import android.util.Log
import androidx.navigation.NavController
import androidx.navigation.findNavController
import com.cryptobucksapp.cryptobucks.utils.base.BaseActivity
import com.cryptobucksapp.cryptobucks.utils.manager.SessionManager

class MainActivity : BaseActivity(R.layout.activity_main){

    private lateinit var navController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        navController = findNavController(R.id.main_nav_host_fragment)

        SessionManager.getInstance().setToken("key_dev_cbf8ad088fbf40b582108d18572646d5")

    }


}


